<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>LIST</title>
</head>
<style>
    fieldset{
        max-width: 50%;

    }
    th{
        align-items: center;
    }
   .name {
        background-color: #04aa6d;
        color: azure;
        padding: 10px 20px;
        margin-right: 25px;
        width: 100px;
        text-align: center;
        border: 1px solid #04aa6d
    }
    .text {
        border: 1px solid #04aa6d;
        padding: 10px;
        width: 278px;
    }
    .industry {
        margin-top: 10px;

    }
    .industryCode{
        width: 300px;
        border: 1px solid #04aa6d;
    }
    .button {
        margin-left: 40%;
        width: 100px;
        margin-top: 30px;
        height: 30px;
        background-color: #04aa6d;
        border: 1px solid #6aebf7;
        border-radius: 5px;
        color: white;
        transition-duration: 0.4s;

    }
    .action {
        width: 50px;
        margin-top: 10px;
        height: 30px;
        background-color: #04aa6d;
        border: 1px solid #6aebf7;
        color: white;
        transition-duration: 0.4s;

    }
    .plus {
        width: 70px;
        margin-left: 70%;
        height: 30px;
        background-color: #04aa6d;
        border: 1px solid #6aebf7;
        border-radius: 5px;
        color: white;
        transition-duration: 0.4s;

    }
    .style {
        display: flex;
    }
    .text {
        border: 1px solid #04aa6d;
    }
    .table {
        margin-top: 30px;
        border-collapse: separate;
        border-spacing: 15px 10px;
        width: 100%;
    }
    .search {
        margin-top: 20px;
    }
    .button:hover {
        opacity: 0.8;
    }
</style>
<body>
    <form method="post" action="">
    <fieldset>
            <div class="industry">
                <div class="style">
                    <div class="name">
                        <label for="">
                            Phân Khoa
                        </label>
                    </div>
                    <select class="industryCode" name="industryCode" id="industryCode">
                        <?php 
                        $industryCode=array(""=>"","MAT"=>"Khoa học máy tính","KDL"=>"Khoa học vật liệu");
                            foreach($industryCode as $i=>$i_value){
                                echo"<option>".$i_value."</option>";
                            }
                        ?>
                    </select>
                </div>
                <div class="industry">
                    <div class="style">
                        <div class="name">
                            <label>
                                Từ Khóa
                            </label>
                        </div>
                        <input type="text" class="text" id="key" name="key">
                    </div>
                </div>
            </div>
            <input type="submit" name="search" class="button" value="Tìm Kiếm">
            <div>
                <div>
                    <div class="search">
                        <label>
                            Số sinh viên tìm thấy XXX
                        </label>
                        <?php
                        if (isset($_POST["plus"]))
                        {
                            header("Location: day5.php");
                        }
                        ?>
                    </div>
                    <input type="submit" class="plus" name="plus" value="Thêm">
                </div>
            </div>
            <div>
                <table class="table">
                    <tr>
                        <th>No</th>
                        <th>Tên sinh viên</th>
                        <th>Khoa</th>
                        <th>Action</th>
                    </tr>

                    <tr>
                        <td>1</td>
                        <td>Nguyễn Văn A</td>
                        <td>Khoa hoc may tinh</td>
                        <td> <input type="submit" name="search" class="action" value="Sửa"></td>
                        <td><input type="submit" name="search" class="action" value="Xóa"></td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Trần Thị B</td>
                        <td>Khoa học máy tính</td>
                        <td> <input type="submit" name="search" class="action" value="Sửa"> </td>
                        <td> <input type="submit" name="search" class="action" value="Xóa"> </td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Nguyễn Hoàng C</td>
                        <td>Khoa học máy tính</td>
                        <td> <input type="submit" name="search" class="action" value="Sửa"> </td>
                        <td> <input type="submit" name="search" class="action" value="Xóa"> </td>

                    </tr>
                    <tr>
                        <td>4</td>
                        <td>Đinh Quang D</td>
                        <td>Khoa học máy tính</td>
                        <td> <input type="submit" name="search" class="action" value="Sửa"> </td>
                        <td> <input type="submit" name="search" class="action" value="Xóa"> </td>
                    </tr>
                </table>
            </div>
        </form>
        </fieldset>
</body>
</html>