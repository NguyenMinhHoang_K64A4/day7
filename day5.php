<!DOCTYPE html>
<html lang="vn">
    <head>
        <meta charset="UTF-8">
    </head>
    <title>Sign Up</title>
    <body>
        
        <fieldset style='max-width: 500px; max-height: 600px; width:auto; height: auto; margin: auto; margin-top: 20px; boder:#1E90FF solid'>
        <?php
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $name = $gender = $industryCode = $file = $brithOfDate = $address="";
        session_start();

        if($_SERVER["REQUEST_METHOD"]== "POST") {
            
            $test=true;
            if(empty(inputHandling($_POST["name"]))) {
                echo "<div style='color: red;'>Hãy nhập họ tên</div>";
                $test=false;
            }
            if (empty($_POST["gender"])) {
                echo "<div style='color: red;'>Hãy chọn giới tính</div>";
                $test=false;
            }
            if(empty(inputHandling($_POST["industryCode"]))){
                echo "<div style='color: red;'>Hãy chọn khoa</div>";
                $test=false;
            }   
            if(empty(inputHandling($_POST["birthOfDate"]))){
                echo "<div style='color: red;'>Hãy nhập ngày sinh</div>";
                $test=false;
            }
            elseif (!validateDate($_POST["birthOfDate"])) {
                echo "<div style='color: red;'>Hãy nhập ngày sinh đúng định dạng</div>";
                $test=false;
            }
            if($test){
                $_SESSION=$_POST;
                header("location: submit.php");
            }
        }
        function inputHandling($data) {
            $data = trim($data);
            $data = stripslashes($data);
            return $data;
        }
        function validateDate($date){
            if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date)) {
                return true;
            } else {
                return false;
            }
        }
        ?>
        <form style='margin: 20px 50px 0 30px' method="post" enctype="multipart/form-data">
    <table style = 'border-collapse:separate; border-spacing:15px 15px;'>
            <tr height = '40px'>
                <td width = 30% style = 'background-color: #1e8dcc; 
                vertical-align: top; text-align: center; padding: 5px 5px'>
                    <label style='color: white;'>Họ và tên<span style='color:red;'>*</span></label>
                </td>
                <td width = 30% >
                    <input type='text' name = "name" style = 'line-height: 32px; border-color:#82cd79'>
                </td>
            </tr>
            <tr height = '40px'>
                <td width = 30% style = 'background-color: #1e8dcc; vertical-align: top; text-align: center; padding: 5px 5px'>
                    <label style='color: white;'>Giới tính<span style='color:red;'>*</span></label>
                </td>
                <td width = 30% >
                <?php
                    $gender=array("Nam","Nữ");
                    for($i = 0; $i < count($gender); $i++){
                        echo"
                            <label class='container'>
                                <input type='radio' value=".$gender[$i]." name='gender'>"
                                .$gender[$i]. 
                            "</label>";
                    }
                ?>  

                </td>
            </tr>
            <tr height = '40px'>
                <td style = 'background-color: #2E8BC0; vertical-align: top; text-align: center; padding: 5px 5px'>
                    <label style='color: white;'>Phân Khoa<span style='color:red;'>*</span></label>
                </td>
                <td height = '40px'>
                    <select name='industryCode' style = 'border-color:#82cd79;height: 100%;width: 80%;'>
                        <?php
                            $industryCode=array(""=>"","MAT"=>"Khoa học máy tính","KDL"=>"Khoa học vật liệu");
                            foreach($industryCode as $i=>$i_value){
                                echo"<option>".$i_value."</option>";
                            }
                        ?>
                    </select>
                </td>
            </tr>
            <tr height = '40px'>
                <td style = 'background-color: #2E8BC0; vertical-align: top; text-align: center; padding: 5px 5px'>
                    <label style='color: white;'>Ngày sinh<span style='color:red;'>*</span></label>
                </td>
                <td height = '40px'>
                    <input type='date' name="birthOfDate" data-date="" data-date-format="DD MMMM YYYY" style = 'line-height: 32px; border-color:#82cd79'>
                </td>
            </tr>
            <tr height = '40px'>
                <td style = 'background-color: #2E8BC0; vertical-align: top; text-align: center; padding: 5px 5px'>
                    <label style='color: white;'>Địa chỉ</label>
                </td>
                <td height = '40px'>
                    <input type='text' name="direction" style = 'line-height: 32px; border-color:#82cd79'> 
                </td>
            </tr>
            <tr height = '40px'>
                <td style = 'background-color: #2E8BC0; vertical-align: top; text-align: center; padding: 5px 5px'>
                    <label style='color: white;'>Hình ảnh</label>
                </td>
                <td height = '40px'>
                    <input type="file" name="file"/>
                </td>
            </tr>
        </table>
        <input type="submit" name="load" value="Upload" style='background-color: #49be25; border-radius: 10px;
        width: 35%; height: 43px; border-width: 0; margin: 30px 130px; text-align: center; color: white;'>

        <?php
        if (isset($_POST['load'])){
            $allow_exttension = array('gif', 'png', 'jpg','jpeg');
            $file_exttension = pathinfo($file, PATHINFO_EXTENSION);
            $file = basename($_FILES['file']['name']);
            $extension = strrpos($file, '.'); 
            $thumb = substr($file, 0, $extension) . '_' .date("YmdHis") . substr($file, $extension);
            $target_dir = "image/";
            // Kiểm tra sự tồn tại của thư mục
            if(!file_exists($target_dir)){
                mkdir($target_dir);
            }              
            //$target_file là đường dẫn file 
            $target_file = $target_dir . $thumb;
            if(isset($_FILES['file'])){
                if(in_array($file_exttension, $allow_exttension) == false && $_FILES['file']['error'] > 0){
                    echo"<p style='color: red';>file upload fail</p>";
                }
                else{
                    move_uploaded_file($_FILES['file']['tmp_name'],$target_file);
                    echo 'file upload';
                }
            }
            else{
                echo 'choose file';
            }
            $_SESSION['file']=$target_file;
        }
        ?>
    </form>
    </fieldset>
</body>
</html>



























